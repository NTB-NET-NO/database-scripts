IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'ntb_archive')
	DROP DATABASE [ntb_archive]
GO

CREATE DATABASE [ntb_archive]  ON (NAME = N'ntb_archive_Data', FILENAME = N'F:\Microsoft SQL Server\MSSQL\data\ntb_archive_Data.MDF' , SIZE = 5867, FILEGROWTH = 10%) LOG ON (NAME = N'ntb_archive_Log', FILENAME = N'F:\Microsoft SQL Server\MSSQL\data\ntb_archive_Log.LDF' , SIZE = 1550, FILEGROWTH = 10%)
 COLLATE Danish_Norwegian_CI_AS
GO

exec sp_dboption N'ntb_archive', N'autoclose', N'false'
GO

exec sp_dboption N'ntb_archive', N'bulkcopy', N'false'
GO

exec sp_dboption N'ntb_archive', N'trunc. log', N'true'
GO

exec sp_dboption N'ntb_archive', N'torn page detection', N'true'
GO

exec sp_dboption N'ntb_archive', N'read only', N'false'
GO

exec sp_dboption N'ntb_archive', N'dbo use', N'false'
GO

exec sp_dboption N'ntb_archive', N'single', N'false'
GO

exec sp_dboption N'ntb_archive', N'autoshrink', N'false'
GO

exec sp_dboption N'ntb_archive', N'ANSI null default', N'false'
GO

exec sp_dboption N'ntb_archive', N'recursive triggers', N'false'
GO

exec sp_dboption N'ntb_archive', N'ANSI nulls', N'false'
GO

exec sp_dboption N'ntb_archive', N'concat null yields null', N'false'
GO

exec sp_dboption N'ntb_archive', N'cursor close on commit', N'false'
GO

exec sp_dboption N'ntb_archive', N'default to local cursor', N'false'
GO

exec sp_dboption N'ntb_archive', N'quoted identifier', N'false'
GO

exec sp_dboption N'ntb_archive', N'ANSI warnings', N'false'
GO

exec sp_dboption N'ntb_archive', N'auto create statistics', N'false'
GO

exec sp_dboption N'ntb_archive', N'auto update statistics', N'false'
GO

use [ntb_archive]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[addNewArticleArchive]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[addNewArticleArchive]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[addSubRelArchive]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[addSubRelArchive]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[checkAccessRights]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[checkAccessRights]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[deleteFromMedieArchive]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[deleteFromMedieArchive]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fetchForSearch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[fetchForSearch]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fetchForSoundSearch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[fetchForSoundSearch]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sendImportToArchive]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sendImportToArchive]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ARTICLES_WITH_SUBREL]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[ARTICLES_WITH_SUBREL]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ARTICLES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ARTICLES]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ARTICLES_IMP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ARTICLES_IMP]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SUBREL]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SUBREL]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SUBREL_IMP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SUBREL_IMP]
GO

CREATE TABLE [dbo].[ARTICLES] (
	[RefID] [int] NOT NULL ,
	[CreationDateTime] [datetime] NOT NULL ,
	[Urgency] [tinyint] NOT NULL ,
	[HasSounds] [tinyint] NOT NULL ,
	[HasPictures] [tinyint] NOT NULL ,
	[IsPrivate] [int] NOT NULL ,
	[Maingroup] [int] NOT NULL ,
	[Subgroup] [int] NOT NULL ,
	[Categories] [int] NOT NULL ,
	[Subcategories] [bigint] NOT NULL ,
	[HasReferences] [int] NOT NULL ,
	[ArticleTitle] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[Ingress] [varchar] (1024) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[ArticleXML] [text] COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[ReceivedDateTime] [datetime] NOT NULL ,
	[Evloc] [int] NOT NULL ,
	[CountyDist] [int] NULL ,
	[Debug] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NULL ,
	[NTB_ID] [varchar] (32) COLLATE Danish_Norwegian_CI_AS NULL ,
	[KeywordID] [int] NULL ,
	[ntbFolderID] [int] NULL ,
	[SoundFileName] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NULL ,
	[KeyWord] [varchar] (16) COLLATE Danish_Norwegian_CI_AS NULL ,
	[isHidden] [tinyint] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[ARTICLES_IMP] (
	[RefID] [int] IDENTITY (793700, 1) NOT NULL ,
	[CreationDateTime] [datetime] NOT NULL ,
	[Urgency] [tinyint] NOT NULL ,
	[HasSounds] [tinyint] NOT NULL ,
	[HasPictures] [tinyint] NOT NULL ,
	[IsPrivate] [int] NOT NULL ,
	[Maingroup] [int] NOT NULL ,
	[Subgroup] [int] NOT NULL ,
	[Categories] [int] NOT NULL ,
	[Subcategories] [bigint] NOT NULL ,
	[HasReferences] [int] NOT NULL ,
	[ArticleTitle] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[Ingress] [varchar] (1024) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[ArticleXML] [text] COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[ReceivedDateTime] [datetime] NOT NULL ,
	[Evloc] [int] NOT NULL ,
	[CountyDist] [int] NULL ,
	[Debug] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NULL ,
	[NTB_ID] [varchar] (32) COLLATE Danish_Norwegian_CI_AS NULL ,
	[KeyWordID] [int] NULL ,
	[ntbFolderID] [int] NULL ,
	[SoundFileName] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NULL ,
	[KeyWord] [varchar] (16) COLLATE Danish_Norwegian_CI_AS NULL ,
	[isHidden] [tinyint] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[SUBREL] (
	[RelID] [int] NOT NULL ,
	[ArticleID] [int] NOT NULL ,
	[Categories] [int] NOT NULL ,
	[Subcategories] [bigint] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SUBREL_IMP] (
	[RelID] [int] IDENTITY (1, 1) NOT NULL ,
	[ArticleID] [int] NOT NULL ,
	[Categories] [int] NOT NULL ,
	[Subcategories] [bigint] NOT NULL 
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW ARTICLES_WITH_SUBREL AS 
( SELECT A.RefID, A.ArticleTitle, A.Ingress, A.CreationDateTime, A.Maingroup, A.Subgroup, A.Categories, A.SubCategories, A.Evloc, A.CountyDist, A.isHidden
FROM ARTICLES A ) 
UNION 
( SELECT A.RefID, A.ArticleTitle, A.Ingress, A.CreationDateTime, A.Maingroup, A.Subgroup, B.Categories, B.Subcategories, A.Evloc, A.CountyDist, A.isHidden
FROM ARTICLES A, SUBREL B 
WHERE A.RefID = B.ArticleID ) 



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE addNewArticleArchive
	@CreationDateTime datetime,
	@Urgency tinyint,
	@HasSounds tinyint,
	@HasPictures tinyint,
	--@IsPrivate tinyint,
	@Maingroup int,
	@Subgroup int,
	@Categories int,
	@Subcategories bigint,
	@HasReferences int,
	@ArticleTitle varchar(255),
	@Ingress varchar(1024),
	@ArticleXML text,
	@Evloc int,
	@CountyDist int,
	@Debug varchar(255) = NULL,
	@NTB_ID varchar(32),
	@KeywordID int,
	@NtbFolderID int,
	@SoundFileName varchar(255),
	@KeyWord varchar(16),
	@isHidden tinyint

AS
SET NOCOUNT ON
SET ANSI_PADDING OFF
declare @isPrivate AS INT
SET @isPrivate = @Maingroup

IF @HasSounds > 0 SET @isPrivate = @isPrivate | 0x4000

-- Add SPORT to Categories if Maingroup is SPORT:
IF @Maingroup = 4 SET @Categories = @Categories | 2048

-- SHIFT bits 16 pos to left:
SET @isPrivate = @isPrivate  * 65536

BEGIN TRAN

-- insert article into current database
INSERT INTO ARTICLES_IMP (CreationDateTime, Urgency, HasSounds, HasPictures, IsPrivate, Maingroup, Subgroup, Categories, Subcategories, HasReferences, ArticleTitle, Ingress, ArticleXML, Evloc, CountyDist, Debug, NTB_ID, KeywordID, NtbFolderID, SoundFileName, KeyWord, isHidden)
	 VALUES (@CreationDateTime, @Urgency, @HasSounds, @HasPictures, @isPrivate, @Maingroup, @Subgroup, @Categories, @Subcategories, @HasReferences, @ArticleTitle, @Ingress, @ArticleXML, @Evloc, @CountyDist, @Debug, @NTB_ID, @KeywordID, @NtbFolderID, @SoundFileName, @KeyWord, @isHidden)

IF @@ERROR <> 0
BEGIN
	-- add error handler here
	RAISERROR ('addNewArticleArchive: Unable to add article to the Article archive import database.', 16,1) WITH LOG
	ROLLBACK TRAN
	SELECT 0 AS 'RefID'
END

ELSE
BEGIN
	COMMIT TRAN
   	SELECT IDENT_CURRENT('ARTICLES_IMP') AS 'RefID'
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE addSubRelArchive
@refid AS int,
@cat AS int,
@subcat AS bigint
AS
SET NOCOUNT ON
IF @refid IS NOT NULL AND @cat IS NOT NULL AND @subcat IS NOT NULL
BEGIN
	INSERT INTO SUBREL_IMP (ArticleID, Categories, Subcategories) VALUES (@refid, @cat, @subcat)
	IF @@ERROR <> 0
	BEGIN
		-- add error handler here
		RAISERROR('addSubRelArchive: unable to add subcategory record for article %d.', 16, 1, @refid) WITH LOG
	END
END
ELSE
BEGIN
	RAISERROR('addSubRelArchive: unable to add subcategory record for article %d.', 16, 1, @refid) WITH LOG
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE checkAccessRights
@aid AS int,
@acf AS int
AS
SET NOCOUNT ON
declare @pr AS int
SET @pr = (SELECT IsPrivate FROM ARTICLES WHERE RefID= @aid)
IF @pr IS NULL
BEGIN
-- Log error here since there is no such article
RAISERROR ('NTB_Archive.checkAccessRights: unable to find article %d.',16,1,@aid) WITH LOG
SELECT -1 AS 'Result'
END
IF (@pr & @acf) > 0
SELECT 0 AS 'Result'
ELSE
SELECT 1 AS 'Result'

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE deleteFromMedieArchive
-- Deleting all MeideArkiv-Messages from Archive
AS
declare @deleted AS int, @msg AS varchar(128)
SET NOCOUNT ON
SET ANSI_PADDING OFF

BEGIN TRAN

DELETE FROM ARTICLES
WHERE RefID < 2000000

set @deleted = @@ROWCOUNT

IF @@ERROR = 0 
	BEGIN
		SET @msg  =  'NTB portal database: deleted ' + LTRIM(STR(@deleted)) + ' articles from archive.'
		COMMIT TRAN	
		SELECT @msg
	END
ELSE
BEGIN
	RAISERROR('NTB portal database: couldn''t delete articles from archive.', 10, 1) WITH LOG
	ROLLBACK TRAN
END

-- delete SUBREL table afterwards
BEGIN TRAN

DELETE FROM SUBREL WHERE ArticleID < 2000000
--DELETE FROM SUBREL FROM SUBREL s WHERE NOT EXISTS(SELECT * FROM ARTICLES a WHERE a.RefID = s.ArticleID)

set @deleted = @@ROWCOUNT

IF @@ERROR = 0
	BEGIN
	SET @msg  =  'NTB portal database: deleted ' + LTRIM(STR(@deleted)) + ' relation records from archive.'
		COMMIT TRAN
		SELECT @msg
	END
ELSE
	BEGIN
	RAISERROR('NTB portal database: couldn''t delete records from SUBREL table from archive.', 10, 1) WITH LOG
	ROLLBACK TRAN
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE fetchForSearch
@querystring AS varchar(256),
@startdate AS varchar(64),
@enddate AS varchar(64) = NULL
AS

SET NOCOUNT ON
declare @exestring AS varchar(512)
declare @dtstart AS DateTime
declare @dtend AS DateTime

SET @dtstart = CONVERT(DateTime, @startdate, 101)
IF @enddate IS NOT NULL
SET @dtend = CONVERT(DateTime, @enddate, 101)

--SET @exestring = 'SELECT DISTINCT TOP 300 RefId, ArticleTitle, Ingress, CreationDateTime FROM ARTICLES_WITH_SUBREL WHERE isHidden = 0 AND CreationDateTime >= ''' + CONVERT(VARCHAR,@dtstart, 102) + ''''
SET @exestring = 'SELECT DISTINCT RefId, ArticleTitle, Ingress, CreationDateTime FROM ARTICLES_WITH_SUBREL WHERE isHidden = 0 AND CreationDateTime >= ''' + CONVERT(VARCHAR,@dtstart, 102) + ''''
IF (@querystring IS NOT NULL) AND (@querystring <> '')
SET @exestring = @exestring + ' AND ' + @querystring
IF @enddate IS NOT NULL
SET @exestring = @exestring + ' AND CreationDateTime < ''' + CONVERT(varchar, @dtend+1, 102) + ''''
SET @exestring = @exestring + ' ORDER BY CreationDateTime DESC'

--PRINT(@exestring)

EXECUTE (@exestring)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE fetchForSoundSearch
@querystring AS varchar(256)
 AS

declare @exestring AS varchar(512)

SET @exestring = 
'SELECT RefId, ArticleTitle, Ingress, CreationDateTime, HasSounds, SoundFileName FROM ARTICLES 
WHERE SoundFileName LIKE ''%' + @querystring + '%'' 
AND HasSounds > 0 AND isHidden = 0
ORDER BY CreationDateTime DESC'

EXECUTE (@exestring)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE sendImportToArchive

AS
-- This procedure sends MedieArkiv imported messages to archive database
declare  @moved AS int, @msg AS varchar(128)
--@now AS datetime,
SET NOCOUNT ON
SET ANSI_PADDING OFF
--SET @now = GETDATE() 

BEGIN TRAN

INSERT INTO ARTICLES SELECT * FROM ARTICLES_IMP
--WHERE 

--DELETE FROM ARTICLES_IMP
--WHERE 

set @moved = @@ROWCOUNT

IF @@ERROR = 0 
	BEGIN
		SET @msg  =  'NTB portal database: sent ' + LTRIM(STR(@moved)) + ' articles to archive.'
		EXEC master..xp_logevent  50001, @msg, informational
		COMMIT TRAN
		SELECT @msg
	END
ELSE
	BEGIN
		RAISERROR('NTB portal database: couldn''t send articles to archive.', 10, 1) WITH LOG
		ROLLBACK TRAN
	END

-- move SUBREL table afterwards
BEGIN TRAN

INSERT INTO SUBREL SELECT * FROM SUBREL_IMP 
--s WHERE NOT EXISTS(SELECT * FROM ARTICLES a WHERE a.RefID = s.ArticleID)

--DELETE FROM SUBREL_IMP 
--FROM SUBREL s WHERE NOT EXISTS(SELECT * FROM ARTICLES a WHERE a.RefID = s.ArticleID)

set @moved = @@ROWCOUNT
IF @@ERROR = 0
	BEGIN
		SET @msg  =  'NTB portal database: sent ' + LTRIM(STR(@moved)) + ' relation records to archive.'
		EXEC master..xp_logevent  50001, @msg, informational
		COMMIT TRAN
		SELECT @msg
	END
ELSE
	BEGIN
		RAISERROR('NTB portal database: couldn''t move records from SUBREL table to archive.', 10, 1) WITH LOG
		ROLLBACK TRAN
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

