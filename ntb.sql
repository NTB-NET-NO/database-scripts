IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'ntb')
	DROP DATABASE [ntb]
GO

CREATE DATABASE [ntb]  ON (NAME = N'ntb_Data', FILENAME = N'F:\Microsoft SQL Server\MSSQL\data\ntb_Data.MDF' , SIZE = 100, FILEGROWTH = 10%) LOG ON (NAME = N'ntb_Log', FILENAME = N'F:\Microsoft SQL Server\MSSQL\data\ntb_Log.LDF' , SIZE = 359, FILEGROWTH = 10%)
 COLLATE Danish_Norwegian_CI_AS
GO

exec sp_dboption N'ntb', N'autoclose', N'false'
GO

exec sp_dboption N'ntb', N'bulkcopy', N'false'
GO

exec sp_dboption N'ntb', N'trunc. log', N'false'
GO

exec sp_dboption N'ntb', N'torn page detection', N'true'
GO

exec sp_dboption N'ntb', N'read only', N'false'
GO

exec sp_dboption N'ntb', N'dbo use', N'false'
GO

exec sp_dboption N'ntb', N'single', N'false'
GO

exec sp_dboption N'ntb', N'autoshrink', N'false'
GO

exec sp_dboption N'ntb', N'ANSI null default', N'false'
GO

exec sp_dboption N'ntb', N'recursive triggers', N'false'
GO

exec sp_dboption N'ntb', N'ANSI nulls', N'false'
GO

exec sp_dboption N'ntb', N'concat null yields null', N'false'
GO

exec sp_dboption N'ntb', N'cursor close on commit', N'false'
GO

exec sp_dboption N'ntb', N'default to local cursor', N'false'
GO

exec sp_dboption N'ntb', N'quoted identifier', N'false'
GO

exec sp_dboption N'ntb', N'ANSI warnings', N'false'
GO

exec sp_dboption N'ntb', N'auto create statistics', N'true'
GO

exec sp_dboption N'ntb', N'auto update statistics', N'true'
GO

use [ntb]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tU_KeyWord]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[tU_KeyWord]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tI_KeyWord]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[tI_KeyWord]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NewInsertCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[NewInsertCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[NewSelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[NewSelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[_expireTicker]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[_expireTicker]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[addNewArticle]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[addNewArticle]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[addSubRel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[addSubRel]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[addSubRel_old]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[addSubRel_old]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[addUsageRecord]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[addUsageRecord]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[cleanTickers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[cleanTickers]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[deleteFromNews]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[deleteFromNews]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fetchForSoundSearch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[fetchForSoundSearch]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getAllArticles24]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getAllArticles24]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getAllArticles72]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getAllArticles72]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getAllSound]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getAllSound]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getAllTickers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getAllTickers]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getArticleRange]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getArticleRange]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getArticleStats]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getArticleStats]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getBitName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getBitName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getBitnames]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getBitnames]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getCaseList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getCaseList]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getKeywords]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getKeywords]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getLatestNameID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getLatestNameID]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getLatestRefID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getLatestRefID]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getLocalGeo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getLocalGeo]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getMainGroupHits]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getMainGroupHits]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getNITF_TypeNames]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getNITF_TypeNames]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getNITF_Types]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getNITF_Types]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getNtbFolder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getNtbFolder]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getOrganizationHits]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getOrganizationHits]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getPRMs]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getPRMs]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getRangeBounds]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getRangeBounds]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getSubCats]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getSubCats]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getSubrelRange]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getSubrelRange]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getTickers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getTickers]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getTypeOfName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getTypeOfName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getWeeklyItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getWeeklyItems]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getWeeklyItems_backup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getWeeklyItems_backup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getXML]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getXML]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[newArticle_old]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[newArticle_old]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[newTickerMessage]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[newTickerMessage]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sendToArchive_old]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sendToArchive_old]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[updateArticle]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[updateArticle]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[updateCaseListCache]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[updateCaseListCache]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ARTICLES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ARTICLES]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BITNAMES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[BITNAMES]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[KeyWord]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[KeyWord]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[STATS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[STATS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SUBREL]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SUBREL]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TICKER]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TICKER]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TypeOfName]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TypeOfName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ntbFolder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ntbFolder]
GO

CREATE TABLE [dbo].[ARTICLES] (
	[RefID] [int] IDENTITY (2000000, 1) NOT NULL ,
	[CreationDateTime] [datetime] NOT NULL ,
	[Urgency] [tinyint] NOT NULL ,
	[HasSounds] [tinyint] NOT NULL ,
	[HasPictures] [tinyint] NOT NULL ,
	[IsPrivate] [int] NOT NULL ,
	[Maingroup] [int] NOT NULL ,
	[Subgroup] [int] NOT NULL ,
	[Categories] [int] NOT NULL ,
	[Subcategories] [bigint] NOT NULL ,
	[HasReferences] [int] NOT NULL ,
	[ArticleTitle] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[Ingress] [varchar] (1024) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[ArticleXML] [text] COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[ReceivedDateTime] [datetime] NOT NULL ,
	[Evloc] [int] NOT NULL ,
	[CountyDist] [int] NULL ,
	[Debug] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NULL ,
	[NTB_ID] [varchar] (32) COLLATE Danish_Norwegian_CI_AS NULL ,
	[KeyWordID] [int] NULL ,
	[ntbFolderID] [int] NULL ,
	[SoundFileName] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NULL ,
	[KeyWord] [varchar] (16) COLLATE Danish_Norwegian_CI_AS NULL ,
	[isHidden] [tinyint] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[BITNAMES] (
	[NameID] [int] IDENTITY (1, 1) NOT NULL ,
	[DescriptiveName] [varchar] (64) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[TypeOfName] [tinyint] NOT NULL ,
	[Category] [int] NOT NULL ,
	[Subcategory] [bigint] NULL ,
	[NITF_XPath] [varchar] (128) COLLATE Danish_Norwegian_CI_AS NULL ,
	[NITF_content] [varchar] (64) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[DisplaySort] [int] NULL ,
	[Bitposition] [tinyint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[KeyWord] (
	[KeyWordID] [int] IDENTITY (0, 1) NOT NULL ,
	[KeyWord] [text] COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[Description] [varchar] (32) COLLATE Danish_Norwegian_CI_AS NULL ,
	[DateTimeCreation] [datetime] NOT NULL ,
	[DateTimeActuality] [datetime] NULL ,
	[ListNumber] [tinyint] NOT NULL ,
	[AutonomyThreshold] [int] NULL ,
	[ScanpixID] [varchar] (16) COLLATE Danish_Norwegian_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[STATS] (
	[StatsID] [int] IDENTITY (1, 1) NOT NULL ,
	[RecordDate] [datetime] NOT NULL ,
	[UserID] [nvarchar] (255) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[ArticleID] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[SUBREL] (
	[RelID] [int] IDENTITY (1, 1) NOT NULL ,
	[ArticleID] [int] NOT NULL ,
	[Categories] [int] NOT NULL ,
	[Subcategories] [bigint] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TICKER] (
	[MessageID] [int] IDENTITY (1, 1) NOT NULL ,
	[CreationDateTime] [datetime] NOT NULL ,
	[StopToShow] [datetime] NOT NULL ,
	[MessagePriority] [tinyint] NOT NULL ,
	[MessageText] [varchar] (256) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[MessageOrigin] [varchar] (64) COLLATE Danish_Norwegian_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TypeOfName] (
	[TypeOfName] [tinyint] NOT NULL ,
	[TypeText] [varchar] (32) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[NITF_XPath] [varchar] (255) COLLATE Danish_Norwegian_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ntbFolder] (
	[ntbFolderID] [int] NOT NULL ,
	[folderName] [varchar] (32) COLLATE Danish_Norwegian_CI_AS NOT NULL ,
	[ImportSQL] [bit] NOT NULL ,
	[ImportAutonomy] [bit] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ARTICLES] WITH NOCHECK ADD 
	CONSTRAINT [DF_ARTICLES_Urgency] DEFAULT (5) FOR [Urgency],
	CONSTRAINT [DF_ARTICLES_HasSounds] DEFAULT (0) FOR [HasSounds],
	CONSTRAINT [DF_ARTICLES_HasPictures] DEFAULT (0) FOR [HasPictures],
	CONSTRAINT [DF_ARTICLES_IsPrivate] DEFAULT (0) FOR [IsPrivate],
	CONSTRAINT [DF_ARTICLES_Maingroup] DEFAULT (0xffffffff) FOR [Maingroup],
	CONSTRAINT [DF_ARTICLES_Subgroup] DEFAULT (0xffffffff) FOR [Subgroup],
	CONSTRAINT [DF_ARTICLES_Categories] DEFAULT (0xffffffff) FOR [Categories],
	CONSTRAINT [DF_ARTICLES_Subcategories] DEFAULT (0xffffffffffffffff) FOR [Subcategories],
	CONSTRAINT [DF_ARTICLES_ReceivedDateTime] DEFAULT (getdate()) FOR [ReceivedDateTime],
	CONSTRAINT [DF_ARTICLES_isHidden] DEFAULT (0) FOR [isHidden],
	CONSTRAINT [PK_ARTICLES] PRIMARY KEY  CLUSTERED 
	(
		[RefID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[BITNAMES] WITH NOCHECK ADD 
	CONSTRAINT [DF_BITNAMES_NITF_attrib] DEFAULT ('') FOR [NITF_XPath],
	CONSTRAINT [PK_BITNAMES] PRIMARY KEY  CLUSTERED 
	(
		[NameID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[KeyWord] WITH NOCHECK ADD 
	CONSTRAINT [DF_KeyWord_CreationDateTime] DEFAULT (getdate()) FOR [DateTimeCreation],
	CONSTRAINT [DF_KeyWord_Passive] DEFAULT (0) FOR [ListNumber],
	CONSTRAINT [PK_KeyWord] PRIMARY KEY  CLUSTERED 
	(
		[KeyWordID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[STATS] WITH NOCHECK ADD 
	CONSTRAINT [DF_STATS_RecordDate] DEFAULT (getdate()) FOR [RecordDate],
	CONSTRAINT [PK_STATS] PRIMARY KEY  CLUSTERED 
	(
		[StatsID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[SUBREL] WITH NOCHECK ADD 
	CONSTRAINT [PK_SUBREL] PRIMARY KEY  CLUSTERED 
	(
		[RelID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[TICKER] WITH NOCHECK ADD 
	CONSTRAINT [DF_TICKER_CreationDateTime] DEFAULT (getdate()) FOR [CreationDateTime],
	CONSTRAINT [DF_TICKER_MinutesToShow] DEFAULT (dateadd(minute,3,getdate())) FOR [StopToShow],
	CONSTRAINT [DF_TICKER_MessagePriority] DEFAULT (3) FOR [MessagePriority],
	CONSTRAINT [DF_TICKER_MessageText] DEFAULT ('Ticker message') FOR [MessageText],
	CONSTRAINT [PK_TICKER] PRIMARY KEY  CLUSTERED 
	(
		[MessageID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[TypeOfName] WITH NOCHECK ADD 
	CONSTRAINT [PK_TypeOfName] PRIMARY KEY  CLUSTERED 
	(
		[TypeOfName]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[ntbFolder] WITH NOCHECK ADD 
	CONSTRAINT [PK_ntbFolder] PRIMARY KEY  CLUSTERED 
	(
		[ntbFolderID]
	)  ON [PRIMARY] 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.NewInsertCommand
(
	@CreationDateTime datetime,
	@Urgency tinyint,
	@HasSounds tinyint,
	@HasPictures tinyint,
	@IsPrivate tinyint,
	@Maingroup int,
	@Subgroup int,
	@Categories int,
	@Subcategories bigint,
	@HasReferences int,
	@ArticleTitle varchar(255),
	@Ingress varchar(1024),
	@ArticleXML text,
	@ReceivedDateTime datetime,
	@Evloc int,
	@CountyDist int,
	@Debug varchar(255)
)
AS
	SET NOCOUNT OFF;
INSERT INTO ARTICLES(CreationDateTime, Urgency, HasSounds, HasPictures, IsPrivate, Maingroup, Subgroup, Categories, Subcategories, HasReferences, ArticleTitle, Ingress, ArticleXML, ReceivedDateTime, Evloc, CountyDist, Debug) VALUES (@CreationDateTime, @Urgency, @HasSounds, @HasPictures, @IsPrivate, @Maingroup, @Subgroup, @Categories, @Subcategories, @HasReferences, @ArticleTitle, @Ingress, @ArticleXML, @ReceivedDateTime, @Evloc, @CountyDist, @Debug);
	SELECT RefID FROM ARTICLES WHERE (RefID = @@IDENTITY)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.NewSelectCommand
AS
	SET NOCOUNT ON;
SELECT RefID, CreationDateTime, Urgency, HasSounds, HasPictures, IsPrivate, Maingroup, Subgroup, Categories, Subcategories, HasReferences, ArticleTitle, Ingress, ArticleXML, ReceivedDateTime FROM ARTICLES

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE _expireTicker
@messageid AS int
AS
SET NOCOUNT ON
IF @messageid <> 0
UPDATE TICKER SET MessagePriority = 0 WHERE MessageID = @messageid

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE addNewArticle
	@CreationDateTime datetime,
	@Urgency tinyint,
	@HasSounds tinyint,
	@HasPictures tinyint,
	--@IsPrivate tinyint,
	@Maingroup int,
	@Subgroup int,
	@Categories int,
	@Subcategories bigint,
	@HasReferences int,
	@ArticleTitle varchar(255),
	@Ingress varchar(1024),
	@ArticleXML text,
	@Evloc int,
	@CountyDist int,
	@Debug varchar(255) = NULL,
	@NTB_ID varchar(32),
	@KeywordID int,
	@NtbFolderID int,
	@SoundFileName varchar(255),
	@KeyWord varchar(16),
	@isHidden tinyint

AS
SET NOCOUNT ON
SET ANSI_PADDING OFF
declare @isPrivate AS INT
SET @isPrivate = @Maingroup
IF @HasSounds > 0 SET @isPrivate = @isPrivate | 0x4000

BEGIN TRAN

-- insert article into current database
INSERT INTO ARTICLES (CreationDateTime, Urgency, HasSounds, HasPictures, IsPrivate, Maingroup, Subgroup, Categories, Subcategories, HasReferences, ArticleTitle, Ingress, ArticleXML, Evloc, CountyDist, Debug, NTB_ID, KeywordID, NtbFolderID, SoundFileName, KeyWord, isHidden)
	 VALUES (@CreationDateTime, @Urgency, @HasSounds, @HasPictures, @isPrivate, @Maingroup, @Subgroup, @Categories, @Subcategories, @HasReferences, @ArticleTitle, @Ingress, @ArticleXML, @Evloc, @CountyDist, @Debug, @NTB_ID, @KeywordID, @NtbFolderID, @SoundFileName, @KeyWord, @isHidden)

IF @@ERROR <> 0
BEGIN
	-- add error handler here
	RAISERROR ('newArticle: Unable to add article to the current database.', 16,1) WITH LOG
	ROLLBACK TRAN
	SELECT 0 AS 'RefID'
END
ELSE
BEGIN
	DECLARE @RefId AS INT
	SET @isPrivate = @isPrivate  * 65536
	SET @RefID = @@IDENTITY
	   
	-- insert article into archive database
	INSERT INTO ntb_archive.dbo.ARTICLES (RefID, CreationDateTime, Urgency, HasSounds, HasPictures, IsPrivate, Maingroup, Subgroup, Categories, Subcategories, HasReferences, ArticleTitle, Ingress, ArticleXML, Evloc, CountyDist, Debug, NTB_ID, KeywordID, NtbFolderID, SoundFileName, KeyWord, isHidden)
	  	 VALUES (@RefId, @CreationDateTime, @Urgency, @HasSounds, @HasPictures, @isPrivate, @Maingroup, @Subgroup, @Categories, @Subcategories, @HasReferences, @ArticleTitle, @Ingress, @ArticleXML, @Evloc, @CountyDist, @Debug, @NTB_ID, @KeywordID, @NtbFolderID, @SoundFileName, @KeyWord, @isHidden)
	   
	IF @@ERROR <> 0
	BEGIN
		-- add error handler here
		RAISERROR ('newArticle: Unable to add article to the archive database.', 16,1) WITH LOG
		ROLLBACK TRAN
		SELECT 0 AS 'RefID'
	END
	ELSE
	-- this will create a ticker message and call ASP page to refresh the memory caches
	-- Added "AND @Subgroup <> 2" by RoV 13.05.2002

	BEGIN
		COMMIT TRAN

		IF @Urgency < 4 AND @NtbFolderID = 1 AND @Subgroup <> 2
	   	BEGIN
	   		declare @tick AS varchar(255), @cl AS char(1), @author As varchar(64)
	   		IF @Urgency = 1 set @cl = 'y'
	   		IF @Urgency = 2 set @cl = 'w'
	   		IF @Urgency = 3 set @cl = 'o'
	   		-- colors look ugly and we don't use this for now, all ticker messages are yellow 
	   		--SET @tick = '/*M:\y'+@ArticleTitle+'*//*U:http://devserver1/ntb/template/x.asp?aid='+LTRIM(STR(@RefId))+',_blank*/'
	   		SET @tick = '/*M:\y'+@ArticleTitle+'*/'
	   		SET @author = '[newArticle] '+LTRIM(STR(@RefID))
	   		EXEC newTickerMessage @tick, @author, @Urgency, 10
	   	END
	   	EXEC master..xp_cmdshell 'E:\sitenotifier.exe http://burundi/ntb/AuthFiles/_refreshListCaches.asp', NO_OUTPUT

	   	SELECT IDENT_CURRENT('ARTICLES') AS 'RefID'
	END 
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE addSubRel
@refid AS int,
@cat AS int,
@subcat AS bigint
AS
SET NOCOUNT ON
IF @refid IS NOT NULL AND @cat IS NOT NULL AND @subcat IS NOT NULL
BEGIN
	INSERT INTO SUBREL (ArticleID, Categories, Subcategories) VALUES (@refid, @cat, @subcat)
	IF @@ERROR <> 0
	BEGIN
		-- add error handler here
		RAISERROR('addSubRel: unable to add subcategory record for article %d.', 16, 1, @refid) WITH LOG
	END
	ELSE
	BEGIN
		DECLARE @RelID AS int
		SET @RelID = @@IDENTITY
		INSERT INTO ntb_archive.dbo.SUBREL (RelID, ArticleID, Categories, Subcategories) VALUES (@RelID, @refid, @cat, @subcat)
		IF @@ERROR <> 0
		BEGIN
			-- add error handler here
			RAISERROR('addSubRel: unable to add subcategory record for article %d to archive.', 16, 1, @refid) WITH LOG
		END
	END
END
ELSE
BEGIN
	RAISERROR('addSubRel: unable to add subcategory record for article %d.', 16, 1, @refid) WITH LOG
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE addSubRel_old
@refid AS int,
@cat AS int,
@subcat AS bigint
AS
SET NOCOUNT ON
IF @refid IS NOT NULL AND @cat IS NOT NULL AND @subcat IS NOT NULL
INSERT INTO SUBREL (ArticleID, Categories, Subcategories) VALUES (@refid, @cat, @subcat)
ELSE
RAISERROR('addSubRel: unable to add subcategory record for article %d.', 16, 1, @refid) WITH LOG

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE [addUsageRecord]
	(@UserID_1 	[nvarchar](255),
	 @ArticleID_2 	[int])
AS INSERT INTO [ntb].[dbo].[STATS] 
	 ( [UserID],
	 [ArticleID]) 
 
VALUES 
	( @UserID_1,
	 @ArticleID_2)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE cleanTickers
AS
UPDATE TICKER SET MessagePriority = 0
WHERE (MessagePriority BETWEEN 0 AND 10 OR MessagePriority BETWEEN 21 AND 255) AND StopToShow < getdate()

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE deleteFromNews
@age AS int = 168
AS
-- This procedure sends outdated messages to archive database, message age must be entered in hours, default is 7 days (168)
declare @now AS datetime, @moved AS int, @msg AS varchar(128)
SET NOCOUNT ON
SET ANSI_PADDING OFF
SET @now = GETDATE() 

BEGIN TRAN

DELETE FROM ARTICLES
WHERE DATEDIFF(hh, CreationDateTime, @now) > @age

set @moved = @@ROWCOUNT

IF @@ERROR = 0 
BEGIN
SET @msg  =  'NTB portal database: deleted ' + LTRIM(STR(@moved)) + ' articles from News.'
EXEC master..xp_logevent  50001, @msg, informational
COMMIT TRAN
END
ELSE
BEGIN
RAISERROR('NTB portal database: couldn''t delete articles from News.', 10, 1) WITH LOG
ROLLBACK TRAN
END
-- move SUBREL table afterwards
BEGIN TRAN
DELETE FROM SUBREL FROM SUBREL s WHERE NOT EXISTS(SELECT * FROM ARTICLES a WHERE a.RefID = s.ArticleID)
set @moved = @@ROWCOUNT
IF @@ERROR = 0
BEGIN
SET @msg  =  'NTB portal database: deleted ' + LTRIM(STR(@moved)) + ' relation records from News.'
EXEC master..xp_logevent  50001, @msg, informational
COMMIT TRAN
END
ELSE
BEGIN
RAISERROR('NTB portal database: couldn''t delete records from SUBREL table.', 10, 1) WITH LOG
ROLLBACK TRAN
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE fetchForSoundSearch
@querystring AS varchar(256)
 AS

declare @exestring AS varchar(512)

SET @exestring = 
'SELECT RefId, ArticleTitle, Ingress, CreationDateTime, HasSounds, SoundFileName FROM ARTICLES 
WHERE SoundFileName LIKE ''%' + @querystring + '%'' 
AND HasSounds > 0 AND isHidden = 0
ORDER BY CreationDateTime DESC'

EXECUTE (@exestring)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE getAllArticles24
AS
declare @now AS datetime
SET @now = getdate()
SELECT RefID, CreationDateTime, Urgency, ArticleTitle,  HasPictures, HasSounds, Ingress, Maingroup, Subgroup, Categories FROM ARTICLES
WHERE
DATEDIFF(hh, CreationDateTime, @now) < 24
AND NtbFolderID = 1
ORDER BY CreationDateTime DESC

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE getAllArticles72
AS
declare @now AS datetime
SET @now = getdate()
SELECT RefID, CreationDateTime, Urgency, ArticleTitle, HasPictures, HasSounds, Ingress, Maingroup, Subgroup, Categories FROM ARTICLES
WHERE
DATEDIFF(hh, CreationDateTime, @now) > 24 AND DATEDIFF(hh, CreationDateTime, @now) < 72
AND NtbFolderID = 1
ORDER BY CreationDateTime DESC

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE getAllSound
AS
declare @now AS datetime
SET @now = getdate()
SELECT RefID, CreationDateTime, Urgency, ArticleTitle, HasPictures, HasSounds, Ingress, Maingroup, Subgroup, Categories FROM ARTICLES
WHERE
DATEDIFF(d, CreationDateTime, @now) < 7 AND HasSounds > 0
ORDER BY CreationDateTime DESC

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getAllTickers
AS
SET NOCOUNT ON
EXEC cleanTickers
SELECT MessageID, CreationDateTime, MessageText, MessagePriority, MessageOrigin FROM TICKER
WHERE MessagePriority > 0
ORDER BY MessagePriority, CreationDateTime

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getArticleRange
@postid AS int = NULL,
@ageindays AS int = 7
AS

-- This procedure will return a range of articles, either all which fit in timeframe or ones which have ID bigger than supplied through the parameter
SET NOCOUNT ON
declare @now AS datetime
set @now = getdate()
declare @ageinmins AS int
set @ageinmins = @ageindays * 24 * 60

IF @postid IS NULL OR @postid = 0
	SELECT RefID, CreationDateTime, Urgency, ArticleTitle, HasPictures, HasSounds, Ingress, IsPrivate, Maingroup, Subgroup, Categories, Subcategories,
		HasReferences, Evloc, CountyDist, SoundFileName, ntbFolderID
	FROM ARTICLES 
	WHERE (DATEDIFF(mi, CreationDateTime, @now) < @ageinmins)
	AND isHidden = 0
	ORDER BY CreationDateTime DESC
ELSE
	SELECT RefID, CreationDateTime, Urgency, ArticleTitle, HasPictures, HasSounds,  Ingress, IsPrivate, Maingroup, Subgroup, Categories, Subcategories,
		HasReferences, Evloc, CountyDist, SoundFileName, ntbFolderID
	FROM ARTICLES
	 WHERE (RefID > @postid AND DATEDIFF(mi, CreationDateTime, @now) < @ageinmins) 
	AND isHidden = 0
	ORDER BY CreationDateTime DESC
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE [getArticleStats] AS

declare @fromDate as DATETIME
declare @toDate as DATETIME
set @fromDate = CONVERT(DATETIME,CONVERT(VARCHAR,getdate()-1,104),104)
set @toDate = CONVERT(DATETIME,CONVERT(VARCHAR,getdate(),104),104)

SELECT     bn.DescriptiveName AS "Stoffgruppe", COUNT(*) AS "Antall artikler"
FROM       BITNAMES bn, ARTICLES ar 
WHERE     (bn.TypeOfName = 0)
AND 	  (bn.Category = ar.Maingroup)
AND 	  isHidden = 0
AND	 ar.CreationDateTime >= @fromDate
AND 	ar.CreationDateTime < @toDate
GROUP BY bn.DescriptiveName
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getBitName
@typeOfName AS int,
@bitPosition AS int,
@category As Int = NULL
AS

DECLARE @strExe As Varchar(512)

SET @strExe = 'SELECT DescriptiveName FROM BITNAMES  WHERE Bitposition=' + Convert(VARCHAR, @bitPosition) + ' AND TypeOfName=' + Convert(VARCHAR, @typeOfName)
IF @category IS NOT NULL
SET @strExe = @strExe + ' AND category = ' + Convert(VARCHAR, @category)

Execute (@strExe)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.getBitnames
AS
	SET NOCOUNT ON;
SELECT NameID, DescriptiveName, TypeOfName, Category, Subcategory, NITF_XPath, NITF_content, DisplaySort FROM BITNAMES

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.getCaseList
/*
	
*/
AS

SELECT * FROM KeyWord WHERE ListNumber > 0 AND Description <> '' ORDER BY ListNumber

RETURN 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.getKeywords
AS
	SET NOCOUNT ON;
SELECT KeyWordID, KeyWord FROM KeyWord

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getLatestNameID
AS
SET NOCOUNT ON
SELECT IDENT_CURRENT('BITNAMES') AS 'MaxID'

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getLatestRefID
AS
SET NOCOUNT ON
SELECT IDENT_CURRENT('ARTICLES') AS 'MaxID'

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getLocalGeo
@countryid AS int = NULL
AS
-- This procedure returns category and subcategory data for article storage, bitwise parameters @catbits and @subcatbits can be supplied as well
SET NOCOUNT ON
IF @countryid IS NOT NULL AND @countryid <> 0
SELECT NameID, DescriptiveName, Bitposition, Subcategory, NITF_XPath, NITF_content FROM BITNAMES
WHERE TypeOfName = 6 AND Category = @countryid
ORDER BY DisplaySort
ELSE
-- check for all regions which could have subdivision and return them - name format will be LocalPlace (Country)
BEGIN
	SELECT a.NameID, a.DescriptiveName+', '+ (SELECT DescriptiveName FROM BITNAMES b WHERE b.Category = a.Category
	AND b.TypeOfName = 5	), a.Subcategory, a.NITF_XPath, a.NITF_content FROM BITNAMES a
	WHERE a.TypeOfName = 6
	ORDER BY DisplaySort
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE [dbo].[getMainGroupHits] AS

declare @fromDate as DATETIME
declare @toDate as DATETIME
set @fromDate = CONVERT(DATETIME,CONVERT(VARCHAR,getdate()-1,104),104)
set @toDate = CONVERT(DATETIME,CONVERT(VARCHAR,getdate(),104),104)

SELECT     bn.DescriptiveName AS "Stoffgruppe", COUNT(*) AS "Antall treff"
FROM         BITNAMES bn INNER JOIN
                      ARTICLES ar ON bn.Category = ar.Maingroup INNER JOIN
                      STATS st ON ar.RefID = st.ArticleID
WHERE     (bn.TypeOfName = 0)
AND	 st.RecordDate >= @fromDate
AND 	st.RecordDate < @toDate
GROUP BY bn.DescriptiveName
ORDER BY bn.DescriptiveName
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getNITF_TypeNames
AS
SET NOCOUNT ON
SET ANSI_PADDING OFF
SELECT TypeOfName, TypeText, NITF_XPath FROM TypeOfName

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getNITF_Types
@typeid AS tinyint,
@catbits AS int = NULL,
@subcatbits AS bigint = NULL
AS
-- This procedure returns category and subcategory data for article storage, bitwise parameters @catbits and @subcatbits can be supplied as well
SET NOCOUNT ON
IF @catbits IS NULL
SELECT NameID, DescriptiveName, Bitposition, Category,  Subcategory, NITF_XPath, NITF_content FROM BITNAMES
WHERE TypeOfName = @typeid
ORDER BY DisplaySort, DescriptiveName
ELSE
-- check for @subcatbits as well
BEGIN
	IF @subcatbits IS NULL
-- no subcat asked
	BEGIN
	SELECT NameID, DescriptiveName, Bitposition, Category, Subcategory, NITF_XPath, NITF_content FROM BITNAMES
	WHERE TypeOfName = @typeid AND (Category & @subcatbits) <> 0 
	ORDER BY DisplaySort, DescriptiveName
	END
	ELSE
-- subcat asked for
	BEGIN
	SELECT NameID, DescriptiveName, Bitposition, Category, Subcategory, NITF_XPath, NITF_content FROM BITNAMES
	WHERE TypeOfName = @typeid AND (Category & @catbits) <> 0 AND Subcategory & @subcatbits <> 0
	ORDER BY DisplaySort, DescriptiveName
	END
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.getNtbFolder
AS
	SET NOCOUNT ON;
SELECT * FROM ntbFolder

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE [dbo].[getOrganizationHits] AS

declare @fromDate as DATETIME
declare @toDate as DATETIME
set @fromDate = CONVERT(DATETIME,CONVERT(VARCHAR,getdate()-1,104),104)
set @toDate = CONVERT(DATETIME,CONVERT(VARCHAR,getdate(),104),104)

SELECT     oo.u_org_name AS "Kunde", COUNT(*) AS "Antall treff"
FROM         ntb_commerce.dbo.OrganizationObject oo INNER JOIN
                      ntb_commerce.dbo.UserObject uo ON oo.g_org_id = uo.g_org_id INNER JOIN
                      STATS st ON uo.g_user_id = st.UserID
WHERE st.RecordDate >= @fromDate
AND st.RecordDate < @toDate
GROUP BY oo.u_org_name
ORDER BY oo.u_org_name
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getPRMs
AS
declare @now AS datetime
declare @ageinhours AS int
set @ageinhours = 24 * 7
SET @now = getdate()
SELECT RefID, CreationDateTime, Urgency, ArticleTitle, HasPictures, HasSounds, Ingress, Maingroup, Subgroup, Categories FROM ARTICLES
WHERE
DATEDIFF(d, CreationDateTime, @now) < @ageinhours AND Maingroup = 32
ORDER BY CreationDateTime DESC
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getRangeBounds
@ageindays AS int = 7
AS
SET NOCOUNT ON
declare @now AS datetime
set @now = getdate()
declare @ageinmins as int
set @ageinmins = @ageindays * 24 * 60

SELECT MIN(RefID) AS 'MinID', MAX(RefID) AS 'MaxID' FROM ARTICLES
WHERE (DATEDIFF(mi, CreationDateTime, @now) < @ageinmins)
	AND isHidden = 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE getSubCats
@catbitposition AS int
AS
SET NOCOUNT ON
declare @catid AS int
set @catid = (SELECT Category FROM BITNAMES WHERE Bitposition = @catbitposition AND TypeOfName=3)
SELECT NameID, DescriptiveName, Bitposition, Subcategory, NITF_XPath, NITF_content FROM BITNAMES
WHERE TypeOfName = 4 AND Category = @catid
ORDER BY DescriptiveName
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getSubrelRange
@postid AS int = NULL,
@minid AS int = NULL
AS

-- This procedure will return a range of subrelated catecories, either all or ones which have ID bigger than supplied through the parameter,
-- or between the first and second parameters
SET NOCOUNT ON
IF @postid IS NULL OR @postid = 0
	SELECT * FROM SUBREL
ELSE
	BEGIN
	IF @minid IS NULL OR @minid = 0
		SELECT * FROM SUBREL 
		INNER JOIN ARTICLES ON SUBREL.ArticleID = ARTICLES.RefID
		WHERE (ArticleID > @postid)
		AND (ARTICLES.isHidden = 0)
	ELSE
		SELECT * FROM SUBREL 
		INNER JOIN ARTICLES ON SUBREL.ArticleID = ARTICLES.RefID
		WHERE (ArticleID BETWEEN @minid AND @postid)
		AND (ARTICLES.isHidden = 0)
	END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getTickers
AS
SET NOCOUNT ON
EXEC cleanTickers
SELECT MessageText, MessagePriority FROM TICKER
WHERE MessagePriority > 0 AND MessagePriority < 255
ORDER BY MessagePriority, StopToShow
-- Added on 2002 Feb 19 to handle ticker messages which should be read only once
UPDATE TICKER SET MessagePriority = 0 WHERE MessagePriority BETWEEN 11 AND 20

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.getTypeOfName
AS
	SET NOCOUNT ON;
SELECT TypeOfName, TypeText, NITF_XPath FROM TypeOfName

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE getWeeklyItems AS
declare @now datetime
SET @now = getdate()
SELECT RefID, CreationDateTime, Urgency, ArticleTitle, HasPictures, HasSounds, Ingress, Maingroup, Subgroup, Categories, SoundFileName, NtbFolderID, 0 as ListNumber 
FROM ARTICLES
--FROM ARTICLES INNER JOIN KeyWord ON ARTICLES.KeywordID = KeyWord.KeyWordID
WHERE
-- DATEDIFF(d, CreationDateTime, @now) < 7 AND (Maingroup & 58  <> 0 OR Subgroup = 8 OR HasSounds > 0 OR ListNumber > 0)
DATEDIFF(d, CreationDateTime, @now) < 7 AND HasSounds > 0 AND isHidden = 0
ORDER BY CreationDateTime DESC
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE getWeeklyItems_backup AS
declare @now datetime
SET @now = getdate()
SELECT RefID, CreationDateTime, Urgency, ArticleTitle, HasPictures, HasSounds, Ingress, Maingroup, Subgroup, Categories, SoundFileName, KeyWordID 
FROM ARTICLES
WHERE
DATEDIFF(d, CreationDateTime, @now) < 7 AND (Maingroup & 58 <> 0 OR HasSounds > 0 OR KeyWordID > 0)
ORDER BY CreationDateTime DESC
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE getXML
@refid AS int = 0
AS
SET NOCOUNT ON
IF @refid IS NOT NULL AND @refid > 0
BEGIN
	IF EXISTS(SELECT * FROM ARTICLES WHERE RefID = @refid)
	SELECT ArticleXML FROM ARTICLES WHERE RefID = @refid
	ELSE
	BEGIN
		SELECT ArticleXML FROM ntb_archive.dbo.ARTICLES WHERE RefID = @refid
		IF @@ROWCOUNT = 0  RAISERROR('Unable to find article with ID=%d.',10,1,@refid) WITH LOG
	END
END
ELSE
	RAISERROR('Detected incorrect request for article with ID=%d.',10,1,@refid) WITH LOG

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE newArticle_old
	@CreationDateTime datetime,
	@Urgency tinyint,
	@HasSounds tinyint,
	@HasPictures tinyint,
	--@IsPrivate tinyint,
	@Maingroup int,
	@Subgroup int,
	@Categories int,
	@Subcategories bigint,
	@HasReferences int,
	@ArticleTitle varchar(255),
	@Ingress varchar(1024),
	@ArticleXML text,
	@Evloc int,
	@CountyDist int,
	@Debug varchar(255) = NULL,
	@NTB_ID varchar(32),
	@KeywordID int,
	@NtbFolderID int,
	@SoundFileName varchar(255),
	@KeyWord varchar(16),
	@isHidden tinyint

AS
SET NOCOUNT ON
SET ANSI_PADDING OFF
declare @pr AS INT
SET @pr = @Maingroup
IF @HasSounds > 0 SET @pr = @pr | 0x4000
	INSERT INTO ARTICLES (CreationDateTime, Urgency, HasSounds, HasPictures, IsPrivate, Maingroup, Subgroup, Categories, Subcategories, HasReferences, ArticleTitle, Ingress, ArticleXML, Evloc, CountyDist, Debug, NTB_ID, KeywordID, NtbFolderID, SoundFileName, KeyWord, isHidden)
		 VALUES (@CreationDateTime, @Urgency, @HasSounds, @HasPictures, @pr, @Maingroup, @Subgroup, @Categories, @Subcategories, @HasReferences, @ArticleTitle, @Ingress, @ArticleXML, @Evloc, @CountyDist, @Debug, @NTB_ID, @KeywordID, @NtbFolderID, @SoundFileName, @KeyWord, @isHidden)

IF @@ERROR <> 0
BEGIN
	-- add error handler here
	RAISERROR ('newArticle: Unable to add article to the database.', 16,1) WITH LOG
	SELECT 0 AS 'RefID'
END
ELSE
-- this will create a ticker message and call ASP page to refresh the memory caches
-- Added "AND @Subgroup <> 2" by RoV 13.05.2002
BEGIN
	IF @Urgency < 4 AND @NtbFolderID = 1 AND @Subgroup <> 2
	BEGIN
		declare @tick AS varchar(255), @cl AS char(1)
		IF @Urgency = 1 set @cl = 'y'
		IF @Urgency = 2 set @cl = 'w'
		IF @Urgency = 3 set @cl = 'o'
	-- colors look ugly and we don't use this for now, all ticker messages are yellow
		SET @tick = '/*M:\y'+@ArticleTitle+'*//*U:http://devserver1/ntb/template/x.asp?aid='+LTRIM(STR(@@IDENTITY))+',_blank*/'
		EXEC newTickerMessage @tick, '[newArticle]', @Urgency, 20
	END
	EXEC master..xp_cmdshell 'C:\sitenotifier.exe http://devserver1/ntb/AuthFiles/_refreshListCaches.asp', NO_OUTPUT
	SELECT IDENT_CURRENT('ARTICLES') AS 'RefID'
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE newTickerMessage

@what AS varchar(256),
@messageorigin AS varchar(64),
@priority AS tinyint = 5,
@minutestoshow AS int = 3,
@stoptoshow AS datetime = NULL

AS
SET NOCOUNT ON
SET ANSI_PADDING OFF
-- This procedure adds new message to the ticker table. If exact date and time is passed in @stoptoshow parameter this is used, otherwise stop time is calculated.
EXEC cleanTickers
--
-- Message origin is mandatory and normally is supplied by application. If message origin is ok but text is missing then nothing happens and error is not raised.
--
declare @sus AS varchar(64) -- session user, can be NT account or SQL Server login
declare @foo AS varchar(256) -- something is wrong and error string will be constructed
SET @sus = SUSER_SNAME()

IF @messageorigin IS NULL OR RTRIM(@messageorigin) = ''
BEGIN
	RAISERROR('Unable to add new ticker message, no message origin was supplied by user %s.', 10, 1, @sus) WITH LOG
	SET @foo = 'Unsuccessful attempt to insert ticker message from user ' + @sus + '.'
	INSERT INTO TICKER (MessagePriority, StopToShow, MessageText, MessageOrigin) VALUES (255, DATEADD(mi,5,getdate()),@foo, @sus+':[portal.ntb.no/SQL]')
	IF @@ERROR <> 0 RAISERROR('Unable to add new ticker message, error %d was returned to [ntb.dbo.newTickerMessage].', 17, 1, @@ERROR) WITH LOG
	RETURN
END

IF @what IS NOT NULL AND RTRIM(@what) <> '' AND @priority > 0
BEGIN
	IF @stoptoshow IS NULL
		INSERT INTO TICKER (MessagePriority, StopToShow, MessageText, MessageOrigin) VALUES (@priority, DATEADD(mi,@minutestoshow,getdate()),@what, @sus+':'+@messageorigin)
	ELSE
		INSERT INTO TICKER (MessagePriority, StopToShow, MessageText, MessageOrigin) VALUES (@priority, @stoptoshow, @what, @sus+':'+@messageorigin)
		
	IF @@ERROR <> 0 
		RAISERROR('Unable to add new ticker message, error %d was returned to [ntb.dbo.newTickerMessage].', 17, 1, @@ERROR) WITH LOG
	ELSE
		EXEC master..xp_cmdshell 'E:\sitenotifier.exe http://BURUNDI/ntb/ticker/_updatetickercontent.asp', NO_OUTPUT
	--RETURN 0
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE sendToArchive_old
@age AS int = 336
AS
-- This procedure sends outdated messages to archive database, message age must be entered in hours, default is 14 days (336)
declare @now AS datetime, @moved AS int, @msg AS varchar(128)
SET NOCOUNT ON
SET ANSI_PADDING OFF
SET @now = GETDATE() 

BEGIN TRAN

UPDATE ARTICLES SET IsPrivate = IsPrivate * 65536
WHERE DATEDIFF(hh, CreationDateTime, @now) > @age

INSERT INTO ntb_archive.dbo.ARTICLES SELECT * FROM ARTICLES
WHERE DATEDIFF(hh, CreationDateTime, @now) > @age

DELETE FROM ARTICLES
WHERE DATEDIFF(hh, CreationDateTime, @now) > @age

set @moved = @@ROWCOUNT

IF @@ERROR = 0 
BEGIN
SET @msg  =  'NTB portal database: sent ' + LTRIM(STR(@moved)) + ' articles to archive.'
EXEC master..xp_logevent  50001, @msg, informational
COMMIT TRAN
END
ELSE
BEGIN
RAISERROR('NTB portal database: couldn''t send articles to archive.', 10, 1) WITH LOG
ROLLBACK TRAN
END
-- move SUBREL table afterwards
BEGIN TRAN
INSERT INTO ntb_archive.dbo.SUBREL
SELECT * FROM SUBREL s WHERE NOT EXISTS(SELECT * FROM ARTICLES a WHERE a.RefID = s.ArticleID)
DELETE FROM SUBREL FROM SUBREL s WHERE NOT EXISTS(SELECT * FROM ARTICLES a WHERE a.RefID = s.ArticleID)
set @moved = @@ROWCOUNT
IF @@ERROR = 0
BEGIN
SET @msg  =  'NTB portal database: sent ' + LTRIM(STR(@moved)) + ' relation records to archive.'
EXEC master..xp_logevent  50001, @msg, informational
COMMIT TRAN
END
ELSE
BEGIN
RAISERROR('NTB portal database: couldn''t move records from SUBREL table to archive.', 10, 1) WITH LOG
ROLLBACK TRAN
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.updateArticle
	@RefID varchar(32),
	@ArticleTitle varchar(255),
	@Ingress varchar(1024),
	@ArticleXML text
AS

SET NOCOUNT ON
SET ANSI_PADDING OFF

IF EXISTS(SELECT * FROM ntb_archive.dbo.ARTICLES WHERE RefID = @RefID)
	UPDATE ntb_archive.dbo.ARTICLES SET ArticleTitle = @ArticleTitle, Ingress = @Ingress, ArticleXML = @ArticleXML WHERE RefId = @RefID
IF @@ERROR <> 0
BEGIN
	-- Add error handler here
	RAISERROR ('updateArticle: Unable to update article in the archive database.', 16,1) WITH LOG
END

IF EXISTS(SELECT * FROM ARTICLES WHERE RefID = @RefID)
BEGIN
	UPDATE ARTICLES SET ArticleTitle = @ArticleTitle, Ingress = @Ingress, ArticleXML = @ArticleXML WHERE RefId = @RefID
END
IF @@ERROR <> 0
BEGIN
	-- Add error handler here
	RAISERROR ('updateArticle: Unable to update article in the news database.', 16,1) WITH LOG
END
ELSE
BEGIN
	-- Refresh Cache
	EXEC master..xp_cmdshell 'E:\sitenotifier.exe http://burundi/ntb/AuthFiles/_reloadCaches.asp', NO_OUTPUT
	DECLARE @AutonomyUpdatePath varchar(255)
	SET @AutonomyUpdatePath = '"E:\Program Files\NTB\UpdateAutonomy\NTB_UpdateAutonomy.exe" ' + CONVERT(VARCHAR(12), @RefID)
	EXEC master..xp_cmdshell @AutonomyUpdatePath, NO_OUTPUT
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE updateCaseListCache

AS
SET NOCOUNT ON
SET ANSI_PADDING OFF
-- This procedure updates CaseListCashe

EXEC master..xp_cmdshell 'E:\sitenotifier.exe http://burundi/ntb/AuthFiles/_updateCaseList.asp', NO_OUTPUT

--RETURN 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE TRIGGER tU_KeyWord ON dbo.KeyWord 
FOR UPDATE

AS

SET NOCOUNT ON

IF UPDATE (KeyWord)
	EXEC updateCaseListCache

/*
BEGIN
	-- Remove Links to former Keyword
	UPDATE Articles SET Articles.KeyWordID = 0
	FROM inserted
	WHERE 
	Articles.KeyWordID = inserted.KeyWordID

	-- Add Links to new Keyword
	UPDATE Articles SET Articles.KeyWordID =  inserted.KeyWordID
	FROM inserted
	WHERE 
	Articles.KeyWord = inserted.KeyWord
END
*/

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE TRIGGER tI_KeyWord ON dbo.KeyWord 

FOR INSERT

AS

SET NOCOUNT ON

IF UPDATE (KeyWord)
EXEC updateCaseListCache

/*
BEGIN
	-- Add Links to new Keyword
	UPDATE Articles SET Articles.KeyWordID =  inserted.KeyWordID
	FROM inserted
	WHERE 
	Articles.KeyWord = inserted.KeyWord
END
*/

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

